FROM alpine:edge
ENV WEBPROC_VERSION 0.1.7
ENV WEBPROC_URL https://github.com/jpillora/webproc/releases/download/$WEBPROC_VERSION/webproc_linux_amd64.gz
RUN apk add --no-cache curl
RUN curl -sL $WEBPROC_URL | gzip -d - > /usr/local/bin/webproc 
RUN chmod +x /usr/local/bin/webproc
RUN apk add --no-cache dnsmasq
COPY updater.job /etc/cron.d/updater
COPY updater.sh /opt/updater.sh
RUN chmod +x /opt/updater.sh
RUN touch /etc/dnsmasq.d/adblock.conf
EXPOSE 69 69/udp
EXPOSE 53 53/udp
EXPOSE 8053 8080
CMD ["webproc","--config","/etc/dnsmasq.conf","--","dnsmasq","--no-daemon"]
